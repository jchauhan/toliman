
from argparse import ArgumentParser
from IPython.frontend.terminal.embed import InteractiveShellEmbed
from IPython.config.loader import Config
import ConfigParser
import tempfile
import os, os.path
import multiprocessing
import time

from issues import *
from conf import *
import utils


#importing modules
from modules.ig import *
from modules.exploitation import *

from model import *



class Scanner(object):
	def __init__(self, ips, conf):
		self.modules = {}
		self.execution_pipeline = []
		self.issues = Issues()
		self.ips = ips
		self.conf = conf
		self.network = nm_network.Network(ips)
		print "Starting Scan on IP Range %s ..." % self.network.get_ip_ranges()

		#Initialize and Register the modules. Registration will not execute modules by default.
		self.modules["ServiceDiscoveryModule"] = nm_service.ServiceDiscoveryModule(self.network , self.conf)
		self.modules["ServiceFingerprintingModule"] = nm_service.ServiceFingerprintingModule(self.network , self.conf)
		self.modules["DirDiscoveryModule"] = nm_hdfs.DirDiscoveryModule(self.network , self.conf)
		self.modules["MaliciousJobModule"] = nm_jobs.MaliciousJobModule(self.network , self.conf)

		# Create a sequence of modules to be executed in the order they are appended
		self.execution_pipeline.append(self.modules["ServiceDiscoveryModule"])
		self.execution_pipeline.append(self.modules["DirDiscoveryModule"])
		self.execution_pipeline.append(self.modules["MaliciousJobModule"])

	def run(self):
		#Execute the modules in sequence
		for module in self.execution_pipeline:
			print "Running Module %s ..." % module.name
			self.issues.add(module.run())


def main(cmdline_params):
	global scanner

	if options.input:
		input_range = options.input

		if options.config:
			config_file_path = options.config
		else:
			config_file_path = "config.ini"

		conf = Conf(utils.load_conf_file(config_file_path))

		if options.output:
			output_file_path = options.output
		else:
			output_file_path = "output.xml"

		output_file = open(output_file_path, "w") 
		scanner = Scanner(input_range, conf)
		scanner.run()
		#scanner.network.print_network()

	else:
		print "Error: need to provide input ip range"


if __name__ == "__main__" :
	#utils.get_conf("db/services.db")

	parser = ArgumentParser(description="""Hi, I am Toliman. the Worlds First Hadoop Pentesting Tool.  
		You can get some help on how to use using a following command 
		"./scanner -h"
		Enjoy!!

		Some Example Usage :- 
			Run scanner:-
				./scanner.py -o report.xml -d demo.apk
			, Shell Command:-
				./scanner.py -s
			, AWS Deamon:-
				./scanner.py -w conf.ini
			    """) 

	parser.add_argument("input", help="IP or IP Range. Nmap style ip/ip ranges are supported")
	parser.add_argument("-o", "--output", help="Output File Name, Default: Standard Output")
	parser.add_argument("-c", "--config", help="Config file. Some of the options will be overwrited by command line options procvided. ")
	parser.add_argument("-s", "--shell", help="Open Scanner in shell mode, useful for debugging.", action="store_true")

	options = parser.parse_args()
	main(options)

