import inflection
import ConfigParser
from conf import *

def get_conf(conf_file):
	"""
		Util function to convert from db/services0.db to db/services.db file
	"""
	config = ConfigParser.ConfigParser()
	config.read(conf_file)
	write_file = open("db/services2.db", "w")
	config2 = ConfigParser.ConfigParser()

	for section in config.sections():
		print section
		options = config.options(section)
		for option in options:
			new_section = section + "." + option
			config2.add_section(new_section)
			config2.set(new_section, "component" , section)
			config2.set(new_section, "ports" , config.get(section, option))
			config2.set(new_section, "name" , inflection.humanize(option) )
			config2.set(new_section, "description" , inflection.humanize(option) )
	config2.write(write_file)
	write_file.close()


def load_conf_file(conf_file):
	"""
		Util function to load conf file and return a dict		
	"""
	config = ConfigParser.ConfigParser()
	config.read(conf_file)
	return config	


def error(msg):
	"""
		Utility function to Print Errror messgaes
	"""
	print "[ERROR] %s" % msg

def debug(msg):
	"""
		Utility function to Print Debug messgaes
	"""
	print "[DEBUG] %s" % msg

def warn(msg):
	"""
		Utility function to Print Warn messgaes
	"""
	print "[WARN] %s" % msg


