import nmap
from modules.abstract import *
from model import * 



class ServiceDiscoveryModule(AbstractScannerModule):
	"""

	"""
	def __init__(self, network, conf, **attr):
		#Need to solve the following problem
		super(AbstractScannerModule, self).__init__()
		self.network = network 
		self.conf = conf
		self.attr = attr
		self.service_db = self.conf.get_services_db()
		self.name = "ServiceDiscoveryModule"

	def run(self):
		nm = nmap.PortScanner()
		results = nm.scan(self.network.get_ip_ranges(), '1-65535')
		#print results['nmap']
		# print results
		for host in nm.all_hosts():
			# print('----------------------------------------------------')
			# print('Host : %s (%s)' % (host, nm[host].hostname()))
			# print('State : %s' % nm[host].state())
			for proto in nm[host].all_protocols():
				# print('----------')
				# print('Protocol : %s' % proto)
				lport = nm[host][proto].keys()
				# print lport
				lport.sort()
				for port in lport:
					for section in self.service_db.sections():
						# print section
						if str(port) in self.service_db.get(section, "ports"):
							service_name = self.service_db.get(section, "name")
							service_desc = self.service_db.get(section, "description")
							service = nm_asset.Service("%s:%d" % (str(host), port), str(host), port, name=service_name, description=service_desc)
							self.network.add_asset(service)
							print "[FOUND] [START] Hadoop Service: "
							print "\t" + service.to_str()
							print "[END]"

					#print 'port : %d\tstate : %s' % (port, nm[host][proto][port]['state'])
		

class ServiceFingerprintingModule(AbstractScannerModule):
	"""

	"""
	def __init__(self, network, conf, **attr):
		#Need to solve the following problem.
		super(AbstractScannerModule, self).__init__()
		self.network = network 
		self.conf = conf
		self.attr = attr

	def run(self):
		pass


