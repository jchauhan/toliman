from modules.abstract import *
from model import * 
import os

class DirDiscoveryModule(AbstractScannerModule):
	"""
	"""
	def __init__(self, network, conf, **attr):
		super(AbstractScannerModule, self).__init__()
		self.network = network 
		self.conf = conf
		self.attr = attr
		self.service_db = self.conf.get_services_db()
		self.name = "DirDiscoveryModule"

	def run(self):
		os.system("hdfs dfs -ls /")
		os.system("hdfs dfs -ls /user")		
