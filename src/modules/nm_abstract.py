from model import *

class AbstractScannerModule(object):
	"""
		Base class for the scanner modules
	"""
	def __init__(self,  network, conf, **attr):
		self.network = network
		self.conf=conf
		self.attr = attr

	def run(self):
		pass