import utils

class Conf(object):

	def __init__(self, conf):
		self.conf = conf
		self.services_db = False
	
	def set(self, section, key, value):
		self.conf.set(section, key, value)

	def get(self, section, key):
		return self.conf.get(section, key)

	def get_services_db(self):
		if not self.services_db:
			self.services_db  = utils.load_conf_file(self.get('signature', 'services_db_path'))
			for section in self.services_db.sections():
				self.services_db.set(section, "ports", self.services_db.get(section, "ports").split(","))
		return self.services_db

