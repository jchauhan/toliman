
from conf import *
from xml.sax.saxutils import escape

from issues import *

class Issues:
	def __init__(self):
		self.issues = []

	def add(self, issue):
		if issue:
			self.issues.append(issue)

	def get(self):
		return self.issues

	def merge(self, issues):
		self.issues.extend(issues.get())

	def to_xml(self):
		xml = ""
		xml += "<Issues>\n"
		for issue in self.issues:
			xml += issue.to_xml()
		xml += "</Issues>\n"
		return xml


class Issue:
	TITLE = "title"
	KEY = "key"
	CATEGORY = "category"
	SEVERTIY = "severity"
	SUMMARY = "sumamry"
	DESCRIPTION = "desc"
	RECOMMENDATION = "recommendation"
	IMPACT = "impact"
	REFERENCES = "references"

	def __init__(self, category, key, title, severity, summary, description, impact, recommendation, references, locations=None):
		self.category = category
		self.key = key
		self.title = title
		self.severity = severity
		self.summary = summary
		self.description = description
		self.recommendation = recommendation
		self.impact = impact
		self.references = references
		self.locations = locations or []

	def to_xml(self):
		xml = ""
		xml += "<Issue>\n"
		xml += "<Severity>%s</Severity>\n" % escape(self.severity)
		xml += "<Title>%s</Title>\n" %  escape(self.title)
		xml += "<Key>%s</Key>\n" % escape(self.key)
		xml += "<Category>%s</Category>\n" %  escape(self.category)
		xml += "<Summary>%s</Summary>\n" %  escape(self.summary)
		xml += "<Description>%s</Description>\n" %  escape(self.description)
		xml += "<Impact>%s</Impact>\n" %  escape(self.impact)
		xml += "<Recommendation>%s</Recommendation>\n" %  escape(self.recommendation)
		xml += "<References>\n"
		for source, url in self.references:
			xml += "<Reference>\n"
			xml += "<Source>%s</Source>\n" %  escape(source)
			xml += "<Url>%s</Url>\n" %  escape(url)
			xml += "</Reference>\n"
		xml += "</References>\n"  
		if(self.locations != None and len(self.locations.locations) > 0):
				xml += self.locations.to_xml()
		xml += "</Issue>\n"
		return xml

	def to_dict(self):
		data = {}
		data["category"] = self.category
		data["severity"] = self.severity
		data["title"] = self.title
		data["status"] = IssueStatus.FAILED
		data["impact"] = self.impact
		data["description"] = self.description
		data["recommendation"] = self.recommendation
		reference = ""
		for source, url in self.references:
			reference += "%s : %s" % (source, url)
		data["reference"] = reference
		return data