
class Issue(object):
	"""
		Issue represent various issues / vulnerabilities in the network
	"""
	def __init__(self, key, title, category, severity, **attr):
		self.key = key
		self.title = title
		self.category = category
		self.attr = attr
		self.severity = severity
