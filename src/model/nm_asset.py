

class AssetType(object):
	"""
		Asset Type in the network 
	"""
	ASSET="asset"
	SERVICE="service"


class Asset(object):
	"""
		Asset class to represent any arbitrary asset in the network
	"""
	def __init__(self, key, **attr):
		self.issues = {}
		self.key = key
		self.type = AssetType.ASSET
		self.attr = attr

	def add_finding(self, issue):
		self.issues[vuln.get_key()] = issue

class Service(Asset):
	"""
		Services in the network 
	"""
	def __init__(self, key, ip, port, **attr):
		#Need to solce this
		super(Asset, self).__init__()
		self.issues = {}
		self.key = key
		self.type = AssetType.ASSET
		self.attr = attr

		self.type = AssetType.SERVICE
		self.ip = ip
		self.port = port

	def to_str(self):
		return "Key %s Type %s IP %s Port %d Attr %s" % (self.key, self.type, self.ip, self.port, self.attr)
