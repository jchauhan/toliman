
class Network:
	def __init__(self, ip_ranges):
		self.assets = {}
		self.ip_ranges = ip_ranges

	def get_ip_ranges(self):
		return self.ip_ranges

	def add_asset(self, asset):
		if not self.get_asset(asset):
			self.assets[asset.key] = asset

	def get_asset(self, asset):
		return self.assets.get(asset)

	def print_network(self):
		print "Network -------------"
		print "Ip Ranges: %s" % self.ip_ranges
		print self.assets
		for asset in self.assets.values():
			print asset.to_str()

